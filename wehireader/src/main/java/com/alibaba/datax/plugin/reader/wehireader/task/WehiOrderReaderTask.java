package com.alibaba.datax.plugin.reader.wehireader.task;

import com.alibaba.datax.common.element.*;
import com.alibaba.datax.common.plugin.RecordSender;
import com.alibaba.datax.common.plugin.TaskPluginCollector;
import com.alibaba.datax.common.util.Configuration;
import com.alibaba.datax.plugin.rdbms.reader.CommonRdbmsReader;
import com.alibaba.datax.plugin.rdbms.util.DataBaseType;
import com.alibaba.datax.plugin.reader.wehireader.enums.ReadColumnEnum;
import com.alibaba.datax.plugin.reader.wehireader.enums.ReadTypeEnum;
import com.alibaba.datax.plugin.reader.wehireader.enums.WriteOrderColumnEnum;
import com.alibaba.datax.plugin.reader.wehireader.enums.WriteOrderDetailColumnEnum;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Optional;

/**
 * @author wyh
 * @date 2022/8/10 18:14
 */
public class WehiOrderReaderTask extends CommonRdbmsReader.Task {

    private static final Logger log = LoggerFactory.getLogger(WehiOrderReaderTask.class);

    private static final String READER_TYPE = "orderReadType";

    private Configuration readerSliceConfig;


    public WehiOrderReaderTask(DataBaseType dataBaseType) {
        super(dataBaseType);
    }

    public WehiOrderReaderTask(DataBaseType dataBaseType, int taskGropuId, int taskId, Configuration readerSliceConfig) {
        super(dataBaseType, taskGropuId, taskId);
        this.readerSliceConfig = readerSliceConfig;
    }

    @Override
    protected Record transportOneRecord(RecordSender recordSender, ResultSet rs,
                                        ResultSetMetaData metaData, int columnNumber, String mandatoryEncoding,
                                        TaskPluginCollector taskPluginCollector) {

        Record record = buildRecord(recordSender, rs, metaData, columnNumber, mandatoryEncoding, taskPluginCollector);

        //write
        String groupId = record.getColumn(ReadColumnEnum.GROUP_ID.getIndex()).asString();
        String shopId = record.getColumn(ReadColumnEnum.SHOP_ID.getIndex()).asString();
        String orderCode = record.getColumn(ReadColumnEnum.ORDER_ID.getIndex()).asString();
        String orderExt = record.getColumn(ReadColumnEnum.ORDER_EXT.getIndex()).asString();
        if (StringUtils.isBlank(orderExt)) {
            recordSender.sendToWriter(record);
            return record;
        }

        //orderExt
        JSONObject orderObj = JSON.parseObject(orderExt);

        //从配置中获取来确定是插入order还是orderDetail
        ReadTypeEnum type = ReadTypeEnum.getByType(readerSliceConfig.getInt(READER_TYPE));
        if (type == ReadTypeEnum.ORDER_DETAIL) {
            orderDetailRecord(record, orderObj, recordSender, groupId, shopId, orderCode);
        } else {
            //默认
            orderRecord(record, orderObj, recordSender, groupId, shopId, orderCode);
        }

        //这个返回值暂时datax中暂时没有用到
        //orderDetail时record是取的最后一个的值
        return record;
    }

    private void orderRecord(Record record, JSONObject orderObj, RecordSender recordSender,
                             String groupId, String shopId, String orderCode) {

        write(record, WriteOrderColumnEnum.GROUP_ID, new StringColumn(groupId));
        write(record, WriteOrderColumnEnum.SHOP_ID, new StringColumn(shopId));
        write(record, WriteOrderColumnEnum.ORDER_CODE, new StringColumn(orderCode));

        String platform = orderObj.getString("platform");
        write(record, WriteOrderColumnEnum.PLATFORM, new StringColumn(platform));

        String reportDate = orderObj.getString("reportDate");
        Date reportDateFinal = null;
        if (StringUtils.isNotBlank(reportDate)) {
            try {
                reportDateFinal = new SimpleDateFormat("yyyyMMdd").parse(reportDate);
            } catch (ParseException e) {
                //ignore
            }
        }
        write(record, WriteOrderColumnEnum.REPORT_DATE, new DateColumn(reportDateFinal));

        String orderTime = orderObj.getString("orderTime");
        Date orderTimeFinal = null;
        if (StringUtils.isNotBlank(orderTime)) {
            try {
                orderTimeFinal = new SimpleDateFormat("yyyyMMddHHmmss").parse(orderTime);
            } catch (ParseException e) {
                //ignore
            }
        }
        write(record, WriteOrderColumnEnum.ORDER_TIME, new DateColumn(orderTimeFinal));

        String platformShopCode = orderObj.getString("platformShopCode");
        write(record, WriteOrderColumnEnum.PLATFORM_SHOP_CODE, new StringColumn(platformShopCode));

        String promotionAmount = orderObj.getString("promotionAmount");
        BigDecimal promotionAmountFinal = StringUtils.isNotBlank(promotionAmount) ? new BigDecimal(promotionAmount) : null;
        write(record, WriteOrderColumnEnum.PROMOTION_AMOUNT, new DoubleColumn(promotionAmountFinal));

        String receiptAmount = orderObj.getString("receiptAmount");
        BigDecimal receiptAmountFinal = StringUtils.isNotBlank(receiptAmount) ? new BigDecimal(receiptAmount) : null;
        write(record, WriteOrderColumnEnum.RECEIPT_AMOUNT, new DoubleColumn(receiptAmountFinal));

        //foodOrder
        JSONObject foodOrder = orderObj.getJSONObject("foodOrder");

        String dinningType = Optional.ofNullable(foodOrder).map(x -> x.getString("dinningType")).orElse(null);
        write(record, WriteOrderColumnEnum.DINNING_TYPE, new StringColumn(dinningType));

        String deskName = Optional.ofNullable(foodOrder).map(x -> x.getString("deskName")).orElse(null);
        write(record, WriteOrderColumnEnum.DESK_NAME, new StringColumn(deskName));

        Integer peopleNumFinal = Optional.ofNullable(foodOrder).map(x -> {
            String peopleNum = x.getString("peopleNum");
            return StringUtils.isNotBlank(peopleNum) ? Integer.valueOf(peopleNum) : null;
        }).orElse(null);
        write(record, WriteOrderColumnEnum.PEOPLE_NUM, new LongColumn(peopleNumFinal));

        String foodOrderType = Optional.ofNullable(foodOrder).map(x -> x.getString("foodOrderType")).orElse(null);
        write(record, WriteOrderColumnEnum.FOOD_ORDER_TYPE, new StringColumn(foodOrderType));

        String paymentName = null;
        String platformPaymentId = null;
        if (foodOrder != null) {
            JSONArray orderPaymentList = foodOrder.getJSONArray("orderPaymentList");
            if (orderPaymentList != null && orderPaymentList.size() > 0) {
                JSONObject firstPayment = orderPaymentList.getJSONObject(0);
                paymentName = firstPayment.getString("paymentName");
                platformPaymentId = firstPayment.getString("platformPaymentId");
            }
        }
        write(record, WriteOrderColumnEnum.PAYMENT_NAME, new StringColumn(paymentName));
        write(record, WriteOrderColumnEnum.PLATFORM_PAYMENT_ID, new StringColumn(platformPaymentId));

        recordSender.sendToWriter(record);
    }

    private void orderDetailRecord(Record record, JSONObject orderObj, RecordSender recordSender,
                                   String groupId, String shopId, String orderCode) {

        JSONObject foodOrder = orderObj.getJSONObject("foodOrder");
        if (foodOrder == null) {
            return;
        }
        JSONArray orderCommodityList = foodOrder.getJSONArray("orderCommodityList");
        if (orderCommodityList == null || orderCommodityList.size() == 0) {
            return;
        }

        write(record, WriteOrderDetailColumnEnum.GROUP_ID, new StringColumn(groupId));
        write(record, WriteOrderDetailColumnEnum.SHOP_ID, new StringColumn(shopId));
        write(record, WriteOrderDetailColumnEnum.ORDER_CODE, new StringColumn(orderCode));

        for (int i = 0; i < orderCommodityList.size(); i++) {

            JSONObject orderDetailObj = orderCommodityList.getJSONObject(i);

            String unitName = orderDetailObj.getString("unitName");
            write(record, WriteOrderDetailColumnEnum.UNIT_NAME, new StringColumn(unitName));

            String styleName = orderDetailObj.getString("styleName");
            write(record, WriteOrderDetailColumnEnum.STYLE_NAME, new StringColumn(styleName));

            String commodityName = orderDetailObj.getString("commodityName");
            write(record, WriteOrderDetailColumnEnum.COMMODITY_NAME, new StringColumn(commodityName));

            String status = orderDetailObj.getString("status");
            write(record, WriteOrderDetailColumnEnum.STATUS, new StringColumn(status));

            String unitPrice = orderDetailObj.getString("unitPrice");
            BigDecimal unitPriceFinal = StringUtils.isNotBlank(unitPrice) ? new BigDecimal(unitPrice) : null;
            write(record, WriteOrderDetailColumnEnum.UNIT_PRICE, new DoubleColumn(unitPriceFinal));

            String discountAmount = orderDetailObj.getString("discountAmount");
            BigDecimal discountAmountFinal = StringUtils.isNotBlank(discountAmount) ? new BigDecimal(discountAmount) : null;
            write(record, WriteOrderDetailColumnEnum.DISCOUNT_AMOUNT, new DoubleColumn(discountAmountFinal));

            recordSender.sendToWriter(record);
        }
    }

    private void write(Record record, WriteOrderColumnEnum columnEnum, Column data) {
        record.setColumn(columnEnum.getIndex(), data);
    }

    private void write(Record record, WriteOrderDetailColumnEnum columnEnum, Column data) {
        record.setColumn(columnEnum.getIndex(), data);
    }
}
