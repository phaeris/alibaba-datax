package com.alibaba.datax.plugin.reader.wehireader.enums;

/**
 * @author wyh
 * @date 2022/8/11 10:59
 */
public enum WriteOrderDetailColumnEnum {

    GROUP_ID("group_id", 0),
    SHOP_ID("shop_id", 1),
    ORDER_CODE("order_code", 2),
    UNIT_NAME("unit_name", 3),
    STYLE_NAME("style_name", 4),
    COMMODITY_NAME("commodity_name", 5),
    STATUS("status", 6),
    UNIT_PRICE("unit_price", 7),
    DISCOUNT_AMOUNT("discount_amount", 8);

    private final String column;

    private final int index;

    WriteOrderDetailColumnEnum(String column, int index) {
        this.column = column;
        this.index = index;
    }

    public String getColumn() {
        return column;
    }

    public int getIndex() {
        return index;
    }
}
