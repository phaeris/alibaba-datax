package com.alibaba.datax.plugin.reader.wehireader.enums;

import java.util.Objects;
import java.util.stream.Stream;

/**
 * @author wyh
 * @date 2022/8/11 14:19
 */
public enum ReadTypeEnum {

    ORDER(1),

    ORDER_DETAIL(2);

    private final Integer type;

    ReadTypeEnum(Integer value) {
        this.type = value;
    }

    public Integer getType() {
        return type;
    }

    /**
     * 根据type获取枚举
     */
    public static ReadTypeEnum getByType(Integer type) {
        ReadTypeEnum[] values = ReadTypeEnum.values();
        return Stream.of(values)
                .filter(v -> Objects.equals(v.getType(), type)).findFirst()
                .orElse(ORDER);
    }
}
