package com.alibaba.datax.plugin.reader.wehireader.enums;

/**
 * @author wyh
 * @date 2022/8/11 10:59
 */
public enum WriteOrderColumnEnum {

    GROUP_ID("group_id", 0),
    SHOP_ID("shop_id", 1),
    PLATFORM("platform", 2),
    DINNING_TYPE("dinning_type", 3),
    ORDER_CODE("order_code", 4),
    REPORT_DATE("report_date", 5),
    ORDER_TIME("order_time", 6),
    PLATFORM_SHOP_CODE("platform_shop_code", 7),
    PROMOTION_AMOUNT("promotion_amount", 8),
    RECEIPT_AMOUNT("receipt_amount", 9),
    DESK_NAME("desk_name", 10),
    PEOPLE_NUM("people_num", 11),
    FOOD_ORDER_TYPE("food_order_type", 12),
    PAYMENT_NAME("payment_name", 13),
    PLATFORM_PAYMENT_ID("platform_payment_id", 14);

    private final String column;

    private final int index;

    WriteOrderColumnEnum(String column, int index) {
        this.column = column;
        this.index = index;
    }

    public String getColumn() {
        return column;
    }

    public int getIndex() {
        return index;
    }
}
