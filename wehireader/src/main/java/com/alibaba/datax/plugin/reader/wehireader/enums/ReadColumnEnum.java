package com.alibaba.datax.plugin.reader.wehireader.enums;

/**
 * @author wyh
 * @date 2022/8/11 10:59
 */
public enum ReadColumnEnum {

    GROUP_ID("group_id", 0),
    SHOP_ID("shop_id", 1),
    ORDER_ID("order_id", 2),
    ORDER_EXT("order_ext", 3);

    private final String column;

    private final int index;

    ReadColumnEnum(String column, int index) {
        this.column = column;
        this.index = index;
    }

    public String getColumn() {
        return column;
    }

    public int getIndex() {
        return index;
    }
}
